package com.example.myapplication.ui.main

import android.Manifest.permission
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.chaquo.python.Python
import kotlinx.android.synthetic.main.fragment_main.*
import ru.yandex.speechkit.*
import java.util.*


class MainFragment : Fragment(), RecognizerListener {
    val py = Python.getInstance()
    val pym = py.getModule("hello")

    private var recognizer: OnlineRecognizer? = null

    companion object {
        fun newInstance() = MainFragment()
        private const val API_KEY_FOR_TESTS_ONLY = "069b6659-984b-4c5f-880e-aaedcfd84102"
        private const val REQUEST_PERMISSION_CODE = 31
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(com.example.myapplication.R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        try {
            SpeechKit.getInstance().init(context!!, API_KEY_FOR_TESTS_ONLY)
            SpeechKit.getInstance().uuid = UUID.randomUUID().toString()
        } catch (ex: Exception) { //do not ignore in a prod version!
        }

        recognizer = OnlineRecognizer.Builder(
            Language.RUSSIAN, OnlineModel.QUERIES,
            this@MainFragment
        ).build()

        start_btn.setOnClickListener { startRecognizer() }
        cancel_btn.setOnClickListener { recognizer!!.cancel() }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode != REQUEST_PERMISSION_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startRecognizer()
        } else {
            updateStatus("Record audio permission was not granted")
        }
    }

    override fun onRecordingBegin(p0: Recognizer) {
        updateStatus("Recording begin")
        updateProgress(0)
        updateResult("")
    }

    override fun onSpeechDetected(p0: Recognizer) {
        updateStatus("Speech detected")
    }

    override fun onSpeechEnds(p0: Recognizer) {
        updateStatus("Speech ends")
    }

    override fun onRecordingDone(p0: Recognizer) {
        updateStatus("Recording done")
    }

    override fun onPowerUpdated(p0: Recognizer, power: Float) {
        updateProgress((power * voice_power_bar!!.max).toInt())
    }

    override fun onPartialResults(p0: Recognizer, recognition: Recognition, eOfU: Boolean) {
        updateStatus("Partial results " + recognition.bestResultText + " endOfUtterrance = " + eOfU)
        if (eOfU) {
            updateResult(recognition.bestResultText)
        }
    }

    override fun onRecognitionDone(p0: Recognizer) {
        updateStatus("Recognition done")
        updateProgress(0)
    }

    override fun onRecognizerError(p0: Recognizer, error: Error) {
        updateStatus("Error occurred $error")
        updateProgress(0)
        updateResult("")
    }

    override fun onMusicResults(p0: Recognizer, p1: Track) {}
    private fun startRecognizer() {
        if (ContextCompat.checkSelfPermission(
                context!!,
                permission.RECORD_AUDIO
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(permission.RECORD_AUDIO),
                REQUEST_PERMISSION_CODE
            )
        } else {
            recognizer!!.startRecording()
        }
    }

    private fun updateResult(text: String) {
        result!!.text = text
        val start = System.currentTimeMillis()
        val pyf = pym.callAttr("test", text.filter { it in ('а'..'я') + ('А'..'Я') + (' ') })
        println("Time for work - " + (System.currentTimeMillis() - start))
        result_norm.text = pyf.toString()
    }

    private fun updateStatus(text: String) {
        current_state!!.text = text
    }

    private fun updateProgress(progress: Int) {
        voice_power_bar!!.progress = progress
    }

}



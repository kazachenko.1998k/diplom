import pymorphy2

morph = pymorphy2.MorphAnalyzer(lang='ru-old')

def test(text):
    ls = ""
    lst = text.split()
    for word in lst:
        p = morph.parse(word)[0]
        ls += p.normal_form + " "
    return ls
